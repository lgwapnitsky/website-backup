#!/usr/bin/env python
# -*- coding: utf-8 -*- #

def imgthumb(image,resolution):
    splitstring = image.split('/')
    splitstring.insert(int(splitstring.index('images'))+1,'t')
    splitstring.insert(int(splitstring.index('images'))+2,str(resolution)+'x')
    return '/'.join(splitstring)
