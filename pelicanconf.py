#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

from fontawesome_markdown import FontAwesomeExtension
from pyembed.markdown import PyEmbedMarkdown

from embedly_cards import EmbedlyCardExtension
from markdown_newtab import NewTabExtension

MD_EXTENSIONS = [PyEmbedMarkdown(),
                 'superscript',
                 FontAwesomeExtension(),
                 'fenced_code',
                 'smarty',
                 'codehilite(css_class=highlight, linenums=False)',
                 'extra',
                 EmbedlyCardExtension(),
                 NewTabExtension(),
                 ]

AUTHOR = u'Larry G. Wapnitsky'
AUTHOR_EMAIL = u'Larry@Wapnitsky.com'
SITENAME = u'Larry G. Wapnitsky'
#SITEURL = "http://larry.wapnitsky.com"
SITEURL = "http://localhost:8000"

SITESUBTITLE = u'a guy who does stuff'
DEFAULT_METADESC = u"Larry G. Wapnitsky's Random Ramblings"

DATE_FORMATS = {
    'en': ('en_US.utf8','%Y-%B-%d'),
}

CACHE_CONTENT = False
LOAD_CONTENT_CACHE = False
CACHE_PATH = 'cache'

PATH = 'content'
STATIC_PATHS= ['images', 'images/reviews',
               'css',
               'js',
               'pdfs',
               'favicon/favicon.ico',
               'deploy',
               'keys',]


EXTRA_PATH_METADATA = { 'favicon/favicon.ico': {'path': 'favicon.ico'},
                        'deploy/git-deploy': {'path': 'git-deploy'},
                        'deploy/deploy.ini': {'path': 'deploy.ini'},
                        'keys/larry_wapnitsky.gpg': {'path': 'larry_wapnitsky.gpg'},
                        'rootfiles/mywot534fb76fc30979f09c4a.html' : {'path' : 'mywot534fb76fc30979f09c4a.html'},
}

PAGE_PATHS = ['pages']

THEME = 'themes/voidy-bootstrap'

# Extra stylesheets, for bootstrap overrides or additional styling.
#STYLESHEET_FILES = ("voidybootstrap_lgw.css","prism.css",)
STYLESHEET_FILES = ("pygments/solarizeddark.css", "voidybootstrap_lgw.css",)
#JAVASCRIPT_FILES = ("prism.js",)
CUSTOM_SCRIPTS_BASE = "lgw_scripts_base.html"
CUSTOM_ARTICLE_FOOTERS = (
    "sharing.html",
    "series.html",
    "taglist.html",
    )

# This settings indicates that you want to create summary at a certain length
SUMMARY_MAX_LENGTH = 50
# This indicates what goes inside the read more link

READ_MORE_LINK = '<span>continue</span>'
# This is the format of the read more link
READ_MORE_LINK_FORMAT = '<a class="read-more" href="/{url}">{text}</a>'

DIRECT_TEMPLATES = ('index', 'categories', 'authors', 'archives')
PAGINATED_DIRECT_TEMPLATES = ['index']

PLUGIN_PATHS = ['pelican-plugins']
PLUGINS = [
    'categorytpl',
    'embedly_cards',
    'feed_summary',
    'google_embed',
    'gravatar',
    'liquid_tags.img',
    'liquid_tags.include_code',
    'liquid_tags.video',
    'liquid_tags.youtube',
    'md_inline_extension',
    'minification',
    'pelican-open_graph',
    'pelican-page-order',
#    'pelican_dynamic',
    'pelican_gist',
#    'photos',
    'series',
    'sitemap',
    'subcategory',
    'summary',
    'thumbnailer',
    'touch',
    'pin_to_top',
]

SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 0.5,
        'indexes': 0.5,
        'pages': 0.5
    },
    'changefreqs': {
        'articles': 'monthly',
        'indexes': 'daily',
        'pages': 'monthly'
    }
}

TYPOGRIFY = True

#DEFAULT_CATEGORY = 'misc'
USE_FOLDER_AS_CATEGORY = True

DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = False
MENUITEMS = [('News', '/category/news/'),
             ('Reviews','/category/reviews/'),
             ('Programming', '/category/programming/'),
             ('About LGW', '/pages/about/'),
             ]

PAGE_URL = 'pages/{slug}/'
PAGE_SAVE_AS = 'pages/{slug}/index.html'
#PAGE_ORDER_BY = 'page_order'

ARTICLE_URL = 'posts/{date:%Y}/{date:%b}/{date:%d}/{slug}.html'
ARTICLE_SAVE_AS = 'posts/{date:%Y}/{date:%b}/{date:%d}/{slug}.html'
ARTICLE_ORDER_BY = 'date'

CATEGORY_URL = "category/{slug}"
CATEGORY_SAVE_AS = "category/{slug}/index.html"

TAG_URL = "tag/{slug}/"
TAG_SAVE_AS = "tag/{slug}/index.html"

AUTHOR_URL = ''
AUTHOR_SAVE_AS = ''

YEAR_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/{date:%b}/index.html'

NEWEST_FIRST_ARCHIVES = True
REVERSE_CATEGORY_ORDER = True

#Thumbnailer settings
IMAGE_PATH = 'images'
THUMBNAIL_DIR = 'images/t'
THUMBNAIL_SIZES = {
    '100x': '100x?',
    '200x': '200x?',
}
THUMBNAIL_KEEP_NAME = True

RESPONSIVE_IMAGES = True

TIMEZONE = 'America/New_York'
DEFAULT_LANG = u'en'


SIDEBAR = 'sidebar.html'
SIDEBAR_HIDE_CATEGORIES = True
SIDEBAR_HIDE_TAGS = True

# Blogroll
#LINKS = (('Pelican', 'http://getpelican.com/'),
#         ('Python.org', 'http://python.org/'),
#         ('Jinja2', 'http://jinja.pocoo.org/'),
#         ('You can modify those links in your config file', '#'),)
LINKS = ()


# Social widget
SOCIAL = (('facebook','https://www.facebook.com/larry.g.wapnitsky', 'fa fa-facebook fa-lg'),
          ('twitter', 'https://twitter.com/lgwapnitsky', 'fa fa-twitter fa-lg'),
          ('youtube', 'https://www.youtube.com/user/lgwapnitsky/', 'fa fa-youtube fa-lg'),
          ('instagram', 'https://instagram.com/larrygwapnitsky/', 'fa fa-instagram fa-lg'),
          ('github', 'https://github.com/lgwapnitsky', 'fa fa-github-alt fa-lg' ),
          ('StackExchange', 'https://stackexchange.com/users/216986/larry-g-wapnitsky', 'fa fa-stack-exchange fa-lg'),
          ('goodreads', 'https://goodreads.com/lgwapnitsky','fa fa-book fa-lg'),
)

GITHUB_URL = 'http://github.com/lgwapnitsky'
TWITTER_CARD = True
TWITTER_USERNAME = 'lgwapnitsky'
GOODREADS_ID = 'lgwapnitsky'
OPEN_GRAPH = True
OPEN_GRAPH_FB_APP_ID = '105212313145043'

DEFAULT_PAGINATION = 10
PAGINATION_PATTERNS = (
    (1, '{base_name}/', '{base_name}/index.html'),
    (2, '{base_name}/page/{number}/', '{base_name}/page/{number}/index.html'),
)

BITLY_USERNAME = 'lgwapnitsky'
BITLY_API_KEY = '6a9f0d7b531f9dffa4a940b6a63e31a509386945'

# Uncomment following line if you want document-relative URLs when developing

RELATIVE_URLS = True
DELETE_OUTPUT_DIRECTORY = False

PHOTO_LIBRARY = '/home/larry/Pictures'
PHOTO_GALLERY = (1024, 768, 80)
PHOTO_ARTICLE = ( 760, 506, 80)
PHOTO_THUMB = (192, 144, 60)
