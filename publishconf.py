#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

import os
import sys

from pelicanconf import *

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

sys.path.append(os.curdir)

# Feed generation is usually not desired when developing
SITEURL = "http://larry.wapnitsky.com"
FEED_DOMAIN = SITEURL
FEED_ALL_ATOM =  'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/%s.atom.xml'
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
FEED_ALL_RSS = None
FEED_USE_SUMMARY = True

RELATIVE_URLS = False
DELETE_OUTPUT_DIRECTORY = False

# Following items are often useful when publishing

GOOGLE_ANALYTICS = 'UA-49725155-1'
DISQUS_SITENAME = 'larrygwapnitsky'
