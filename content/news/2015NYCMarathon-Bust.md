TItle: 2015 NYC Marathon is a no-go
Date: 2015-03-03 20:18
Modified: 2015-03-04 05:40
Tags: running, marathon, races, half-marathon, broad street run, american cancer society, fundraising
Description: My feelings on not getting into the 2015 NYC Marathon
social_image: news/2015NYCMarathonRejection.jpg

<!-- PELICAN_BEGIN_SUMMARY -->
Well, I knew the odds were long, but I was still holding out for a shot. Unfortunately, 2015 does not appear to be the year of the NYC Marathon for me. 

![Inline_NYCM1][NYCMarathon1]

<!-- PELICAN END SUMMARY -->

I can [raise $3400] to get a guaranteed entry with the American Cancer Society, but that's probably an even longer shot.

Fear, not! My plans to run a marathon this year still live, as I'm automatically entered into the Philadelphia Half Marathon via my [Broad Street Run fundraising], and upgrading to the full marathon should not be a problem.

It's time to kick my training into gear now that I've been given the clearance from my doctor after slipping on the ice 3+ weeks ago!

[NYCMarathon2]: news/1031_marathon_630x420.jpg
[NYCMarathon1]: /images/news/2015NYCMarathonRejection.jpg
[raise $3400]:  http://igg.me/at/ZrIAKJTNeCK
[Broad Street Run fundraising]: https://goo.gl/XV5xYY
