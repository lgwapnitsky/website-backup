Title: New Website
Date: 2015-03-02 18:15
Modified: 2015-03-02 18:15
Tags: Intro, New Website
Pin: false

<!-- PELICAN_BEGIN_SUMMARY -->
If you've visited my site before, then you've noticed that everything looks different.

When I switched to a new host, I decided to go simpler on my article development, so I switched to a Static Website Generator called [Pelican](http://getpelican.com). So far, I'm finding it much easier, though transferring my old articles is a bit of hassle.

Stay tuned...!

<!-- PELICAN_END_SUMMARY -->
