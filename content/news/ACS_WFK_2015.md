Title: Giving to charity is as easy as cutting your hair
Date: 2015-04-01
Modified: 2015-04-01
Tags: personal, health, fitness, running, charity, American Cancer Society, Wigs For Kids
Description: Time to hit the reset-button again!
social_image: news/pre-haircut.jpg
Pin: false

<!-- PELICAN_BEGIN_SUMMARY -->

For the second year in a row, I've been raising money for the [American Cancer Society] while leading up to Philadelphia's [Broad Street Run]. This year, I decided to up the stakes by raising my personal fundraising goal AND adding in the [Philadelphia Marathon] as a personal goal. In order to do so, I had to raise the stakes on my fundraising methods. How did I do that?

![Inline_pre-haircut][pre-haircut]

<!-- PELICAN_END_SUMMARY -->

Starting on January 1st, 2015, and ending on February 28, 2105, I put it out to the world the following three milestone goals:

If I raised:

- $500 - I would donate my long hair to [Wigs For Kids] much earlier than planned
- $750 - Donate my hair AND shave off my beard
- $1000 - Donate my hair, shave my beard, AND cut off the rest of my hair down to the scalp

##### [Background - I've been growing my hair since September 2012 and have only had it trimmed once. This was also the first time I've ever grown my hair long in my life]

Long-story-short, I raised $1006 in that timeframe, so today, April 1, 2015, I decided to take care of business.

I made a trip to the wonderful [Heaven And Earth Salon] in Lafayette Hill, PA, as I know they are affiliated with the Wigs For Kids program. My appointment was with Tiffany who, repeatedly, wanted to make sure I really was getting rid of ALL my hair! I assured her that that was the plan.

With the forming of two lovely ponytails, a few clips of scissors, and the buzz of clippers, I was a completely transformed man. The change in appearance has shocked friends and accquaintances, many of whom have never seen me without my long hair. (See the transformation slideshow at the end of the article) 

![Inline_clipping][clipping]

Needless to say, the change is worth it, and I'm definitely planning on doing it again in the next few years.

# BUT!!

I'm not done with charity! My fundraising doesn't stop here. I'm asking you to keep the good vibes going. Please take a minute to visit my [donation link] and toss in a few of those dollars that you may spend on a cup of *$ coffee, or that extra muffin in the morning. Believe me, funding research to give more people birthdays is worth the small sacrifices.


<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="4" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAAGFBMVEUiIiI9PT0eHh4gIB4hIBkcHBwcHBwcHBydr+JQAAAACHRSTlMABA4YHyQsM5jtaMwAAADfSURBVDjL7ZVBEgMhCAQBAf//42xcNbpAqakcM0ftUmFAAIBE81IqBJdS3lS6zs3bIpB9WED3YYXFPmHRfT8sgyrCP1x8uEUxLMzNWElFOYCV6mHWWwMzdPEKHlhLw7NWJqkHc4uIZphavDzA2JPzUDsBZziNae2S6owH8xPmX8G7zzgKEOPUoYHvGz1TBCxMkd3kwNVbU0gKHkx+iZILf77IofhrY1nYFnB/lQPb79drWOyJVa/DAvg9B/rLB4cC+Nqgdz/TvBbBnr6GBReqn/nRmDgaQEej7WhonozjF+Y2I/fZou/qAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://instagram.com/p/08NS_RIxAk/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_top">@americancancer @wigsforkids I feel great! Http://goo.gl/XV5xYY</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">A video posted by Larry G. Wapnitsky (@larrygwapnitsky) on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2015-04-01T17:30:08+00:00">Apr 1, 2015 at 10:30am PDT</time></p></div></blockquote>
<script async defer src="//platform.instagram.com/en_US/embeds.js"></script>

<table width="50%">
<tr>
	<td><img src="/images/news/DNationLogo.jpg" width="200"</td>
	<td><img src="/images/news/wfklogo.jpg" width="200"</td>
</tr>
</table>


[donation link]: https://goo.gl/XV5xYY
[American Cancer Society]: http://cancer.org
[Wigs For Kids]: http://wigsforkids.org
[Broad Street Run]: http://broadstreetrun.com
[Philadelphia Marathon]: http://philadelphiamarathon.com/
[Heaven And Earth Salon]: http://www.heavenearthsalon.com/

[pre-haircut]: /images/news/pre-haircut.jpg
[clipping]: /images/news/clipping.jpg
[dnation logo]: /images/news/DNationLogo.jpg
[wfk logo]: /images/news/wfklogo.jpg

