Title: Time to hit the reset-button again!
Series: 2015 Personal Improvement
Date: 2015-03-09
Modified: 2015-03-10
Tags: personal, health, fitness, running
Description: Time to hit the reset-button again!
social_image: news/stimpy_red_button.jpg

<!-- PELICAN_BEGIN_SUMMARY -->
One year ago, I had bad cholesterol levels and was at my highest weight ever. I'm not at that point right now, but I do need to hit the reset button again after the last few weeks.

![Inline_RedButton][resetbutton]
<!-- PELICAN_END_SUMMARY -->

Since March of 2014, I had lost about 30 lbs., gained some of it back from November-December, then lost it again by the beginning of February. I felt great, I was running, working out hard (CrossFit&#0153; -style) and building some muscle. Then, on February 9^th^, I fell -- literally and figuratively.

One slip on the ice while loading up my car for the morning set me back a month in training. Unfortunately, this set me back mentally as well, and all my bad eating habits came back. All the weight I had lost from the beginning of the year is now back with a vengeance. In regards to training, I now have less than 2 months to train for the [Broad Street Run], in which I was hoping to set a personal record of 90-minutes. My only goal right now is to finish. That should not be an issue, but I'm a little disheartened.

Why am I writing all this out? Mostly because I want to be held accountable. I'm even putting my MyFitnessPal weight ticker on this post so that you can check on me. I know I can do this (this being losing the weight), but I may need some pushes once in awhile.

[![MFP_WLT][WLT_Image]][WLT_Link]

This guy below knows how to crush his goals. Let's get him back in this world! 

He's got a lot to accomplish this year, including:

*    The aforementioned [Broad Street Run]
*    The [Runners' World Hat Trick]
	*    5k and 10k on Saturday
	*    Half-marathon on Sunday
*    The [Philadelphia Marathon]
*    Pacing the [Superhero Half Marathon]

Not only that, but I'm counting on a good friend of mine to kick my ass in terms of my strength building.

Hitting the button in 3...2...1...

![Inline_ProfilePic][ProfilePic]

[resetbutton]: /images/news/stimpy_red_button.jpg
[sub30club]: https://www.facebook.com/groups/223573647748267/
[WLT_Link]: http://www.myfitnesspal.com/weight-loss-ticker
[WLT_Image]: http://tickers.myfitnesspal.com/ticker/show/3597/8685/35978685.png
[Broad Street Run]: http://broadstreetrun.com
[Runners' World Hat Trick]: http://rw.runnersworld.com/rwhalf/
[Philadelphia Marathon]: http://philadelphiamarathon.com
[Superhero Half Marathon]: http://superherohalf.com
[ProfilePic]: /images/news/DNation_workout_2014.jpg
