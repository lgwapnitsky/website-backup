Title: Marathon 2015 - The Tough Decision
Date: 2015-07-15
Modified: 2015-07-15
Tags: running, marathon, races
Description: The tough decision I had to make about the 2015 Philadelphia Marathon

<!-- PELICAN_BEGIN_SUMMARY -->

2015 was going to be the year I finally ran my first marathon. I was excited last year when I made the decision to do so. I had even set up a training plan. Then, life got in the way...

<!-- PELICAN_END_SUMMARY -->

It all started in February with a slip on the ice. I landed on my back, and was out of training for six weeks. Once that was better, I started running again, training for the [Broad Street Run][BSR_Article]. I did run it, but was hampered by bad allergies, which, in turn, caused me to not run too much in advance of that race.

While I DID get a personal best time at BSR, it was not easy, but I felt as though I could kick my training for the marathon off again with no issues. Boy, was I wrong.

Some of the medication I had taken during the allergy issues had some negative repercussions (of which I will not go into detail). This led to a number of personal issues, which, again, took me away from running. With some help, I finally got into the mental state to start again. (can you guess where this is going?)

Long-story-short:

* I stubbed my toe on a stationary bike stand. It's been broken for over 3 weeks now.
* I've been having trouble finding my mojo
* Most importantly, family. **FAMILY COMES FIRST!!!** An important family event is the same day as the Philadelphia Marathon. That was the deciding factor that said this year wasn't mine.

With the combination of factors building up, I believe I made the right decision. I don't feel as though my training would have gotten me in a good position to run, and risking injury is something I'm not willing to do.

Oh, well. There's always next year, the year after that, ad nauseum.

[BSR_Article]: BSR2015.md
