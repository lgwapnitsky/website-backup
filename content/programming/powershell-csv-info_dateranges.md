Title: Breaking down CSV file information by date
Series: PowerShell notes
Date: 2015-03-10
Modified: 2015-03-11
Tags: programming, PowerShell, scripting, IT
Description: Too many files that had dated data that I needed to reconcile to a small range of dates.
social_image: programming/powershell2xa4.jpg

<!-- PELICAN_BEGIN_SUMMARY -->
I had a need this morning to reconcile some reporting data for a large number of projects in Microsoft TFS. I exported the data using Team Foundation Sidekicks, but needed to break down the data by month within a certain range of dates. Here's how I did it.
<!-- PELICAN_END_SUMMARY -->

Let's break it down into steps:

**I needed to bring in all CSV files from my current directory that were greater than 0 bytes.**

```
$files = dir *.csv | where {$_.length -gt 0}
```

Why not bring them all in even if they are 0 bytes? There's no need to use memory to collect unnecessary garbage. Let's avoid it at the beginning rather than wasting processing power and memory.

**Now we need to iterate through this file list and process them one-by-one.**

```
$z = foreach ($file in $files) {
	write-progress -id 1 -activity $file.name -status "processing..."
	$x = import-csv $file
```

`$z` will be used to store the data returned from iterating through every file.

I like to use `write-progress` when I'm iterating through large lists\dictionaries\arrays. It helps me keep track of where I am within that list and see if I need to break out. It's also better than constantly writing to the screen using `write-host`, as that data can wind up in our collection of objects below.

**Extract information from each file**

Now we're going to iterate through records of the file that's been imported. We're going to create a custom object that contains:

* Project Name
* Total commits in the project
* Total commits in
    * November 2014
    * December 2014
    * January 2015
    * February 2015


```
	$y = new-object -type psobject -Property @{
		Project = $file.basename
		Total = $x.count
		Nov2014 = ($x | where {($_.date -ne $null) -and ([datetime]$_.date -gt [datetime]::parse("11-1-2014")) -and ([datetime]$_.date -lt [datetime]::parse("12-1-2014"))}).count
		Dec2014 = ($x | where {($_.date -ne $null) -and ([datetime]$_.date -gt [datetime]::parse("12-1-2014")) -and ([datetime]$_.date -lt [datetime]::parse("1-1-2015"))}).count
		Jan2015 = ($x | where {($_.date -ne $null) -and ([datetime]$_.date -gt [datetime]::parse("1-1-2015")) -and ([datetime]$_.date -lt [datetime]::parse("2-1-2015"))}).count
		Feb2015 = ($x | where {($_.date -ne $null) -and ([datetime]$_.date -gt [datetime]::parse("2-1-2015")) -and ([datetime]$_.date -lt [datetime]::parse("3-1-2015"))}).count
	}
```


**Store the data and report it**

Now we're going to output the created object `$y` and store it in `$z` for reporting.

Following the final file, we will then output the objects in `$z` to a CSV file, specifying `NoTypeInformation` to make the CSV more univerally readable.


```
	$y
}
$z | select Project,Total,Nov2014,Dec2014,Jan2015,Feb2015 | export-csv -NoTypeInformation TFS_Nov2014-Feb2015Activity.csv
```

**Let's put it all together**

When all the above is combined, your script will look like this:

[gist:id=31deaf16727762151bae]

### Summary ###

While this does the job, there are much cleaner, more concise ways of accomplishing the same thing. I needed to burn this off as quickly as possible, so it came out quick and dirty. When I find time, I will update this post with better code. In the meantime, feel free to modify this code and adapt it to your own needs.
