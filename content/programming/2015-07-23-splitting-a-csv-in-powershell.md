Title: Splitting a CSV in PowerShell
Series: PowerShell notes
Date: 2015-07-23
Modified: 2015-07-23
Tags: programming, PowerShell, scripting, IT
social_image: programming/powershell2xa4.jpg

<!-- PELICAN_BEGIN_SUMMARY -->

Today I had a need to split some CSV (comma-separated value) files to run on a program that had limits to the size it could read.  I found a number of solutions out there to do what I wanted in PowerShell, but many were a bit more complex than I needed, so I wrote my own.

<!-- PELICAN END SUMMARY -->


To simplify things for my needs, I hard-coded my major variables, but changing this to a cmdlet with modular parameters won't be that difficult.

```
$csvfile = import-csv "file.csv" -Encoding UTF8

$split = 75
```

Then, using some basic math built in to PowerShell and .NET, I figured out how many files I would be generating, using division, modulo, and floor\ceiling for rounding.  
```
$csvfile_split = $csvfile.length / $split
$floor = [math]::Floor($csvfile_split)
$ceiling = [math]::Ceiling($csvfile_split)
$excess = $csvfile.length % $split
```

We now start the iteration of the imported CSV file and create the proper number of split files. The value in *$excess* lets me know if I need to create an extra file at the end.

```
$start = 0
$end = $split - 1

switch ($excess -gt 0)
{
    $true   { $upper = $ceiling}
    $false  { $upper = $floor }
}

for ($i = 1; $i -le $upper; $i++) {
    $csvfile[$($start)..$($end)] | export-csv "split$($i).csv" -notypeinformation -Encoding UTF8
    $start += $split
    $end += $split

}
```

**Just that simple!**

Here's the full code:

```
$csvfile = import-csv "file.csv" -Encoding UTF8

$split = 75

$csvfile_split = $csvfile.length / $split
$floor = [math]::Floor($csvfile_split)
$ceiling = [math]::Ceiling($csvfile_split)
$excess = $csvfile.length % $split

$start = 0
$end = $split - 1

switch ($excess -gt 0)
{
    $true   { $upper = $ceiling}
    $false  { $upper = $floor }
}

for ($i = 1; $i -le $upper; $i++) {
    $csvfile[$($start)..$($end)] | export-csv "split$($i).csv" -notypeinformation -Encoding UTF8
    $start += $split
    $end += $split

}
```
