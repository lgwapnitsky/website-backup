Title: Tekma Sport - Calf Compression Sleeves
Date: 2015-03-19
Modified: 2015-03-19
Tags: review, sports, clothing, running
social_image: reviews/tekma_sport_sleeves.jpg
series: Calf & Compression Sleeves\Socks

<!-- PELICAN_BEGIN_SUMMARY -->
<a href="http://www.tomoson.com/?code=TOP82bbde847a36d61b6854f24b4670e580" rel="nofollow"><img style="display: none" src="http://www.tomoson.com/images/front/pixel.png" /></a>

This is my third pair of compression sleeves that I've ever owned. Of the three, let me be honest, these were not my favorite, probably due to the size.

![Inline_sleeves][sleeves]

<!-- PELICAN_END_SUMMARY -->

I was sent a size Large from the company to review. Unfortunately, my legs were too small. I had a lot of difficulty getting these sleeves over my legs, and when they were on, they felt very tight.

Quality-wise, though, these are solid. They are made very well, and seem as though they would last a long time with good care.

I feel that, if I had the larger size, I might be a little better inclined to say more about these.  I will contact the manufacturer and see if a replacement can be provided. If so, I plan on updating the review.

In the meantime, they are avaialable from [Amazon][AmazonLink].

<a href="http://www.tomoson.com/?code=BOTTOM82bbde847a36d61b6854f24b4670e580" rel="nofollow"><img style="display: none" src="http://www.tomoson.com/images/front/pixel.png" /></a>

[AmazonLink]: http://smile.amazon.com/Calf-Compression-Sleeves-Triathlons-Performance/dp/B00QWDHJRQ/ref=sr_1_1?ie=UTF8&qid=1426794895&sr=8-1&keywords=tekma+sleeve
[sleeves]: /images/reviews/tekma_sport_sleeves.jpg
