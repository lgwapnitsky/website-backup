Title: Aennon Multi-function LED Headlamp
Date: 2015-03-18
Modified: 2015-03-18
Tags: review, tools, running, lighting
social_image: reviews/aennon_led_headlamp.jpg

<!-- PELICAN_BEGIN_SUMMARY -->
<a href="http://www.tomoson.com/?code=TOP37f6dfa8a0d3030316d928df1b3d4f57" rel="nofollow"><img style="display: none" src="http://www.tomoson.com/images/front/pixel.png" /></a>

When I had the opportunity to review the [Aennon LED Headlamp][AmazonLink], I thought to myself, "I already own one that I use for running and working around the house, why do I need to test another one out?" Well, I'm glad I did, because my existing one has been relegated to indoor household maintenance usage only, and the [Aennon][Aennon] has taken its place as my outdoor go-to headlamp for running and more!

![Inline_Headlamp1][Headlamp_amazon]

<!-- PELICAN_END_SUMMARY -->

Now, it's been cold out, so I haven't been able to test this for running yet, but the cold WAS a great time to test it for something else - shoveling snow.

I have a light above my garage that lights up about half my driveway, as well as porch and lawn lights near my walk in front of house, but when I get closer to the bottom of the driveway, and when I work on my walks, the light there is minimal. Additionally, I'm right at the street, and when people drive in the snow, they need to see me coming (which is why I wear a reflective jacket and\or vest).

Long story short, this light was bright and amazing (much like your very humble reviewer :fa-smile-o: ). I can't wait for it to get a bit warmer out and really test this thing out before the cock crows on my long training runs.

I highly recommend this product, and suggest you get your own [here][AmazonLink].

<a href="http://www.tomoson.com/?code=BOTTOM37f6dfa8a0d3030316d928df1b3d4f57" rel="nofollow"><img style="display: none" src="http://www.tomoson.com/images/front/pixel.png" /></a>


[Headlamp_amazon]: /images/reviews/aennon_led_headlamp.jpg
[Aennon]: http://aennon.com
[AmazonLink]: http://smile.amazon.com/Best-LED-Headlamp-Flashlight-Money-Back-Guarantee/dp/B00SR20JLS/ref=sr_1_13?s=sporting-goods&ie=UTF8&qid=1426601605&sr=1-13&keywords=headlamps
