Title: Yorkberg Compression Socks
Date: 2015-03-19
Modified: 2015-03-20
Tags: review, sports, clothing, running
social_image: reviews/yorkberg_myfoot.jpg
series: Calf & Compression Sleeves\Socks

<!-- PELICAN_BEGIN_SUMMARY -->
As a runner, I care a lot about my health, and my legs are one of the most important things to watch out for. One thing I've learned is that leg compression while running (and afterward) is very important for blood flow and muscle maintenance. So, when I was given the opportunity to review [Yorkberg Compression Socks][AmazonLink], I was excited to see how they would work out, having only worn sleeves before.

![Inline_Packaging][packaging]
<!-- PELICAN_END_SUMMARY -->

I'm not sure if was expecting these to be so much like a dress sock, so I was hesitant at first to wear them when going out for a run. Boy was I surprised when I finally got them on!

First things first, these were a bit on the tight side (yes, Larry, they're compression socks). I attribute it to having larger calves. Once I got moving while running, it didn't make a difference. These were so comfortable, I practically forgot that I was wearing them!

![Inline_myfoot][myfoot]

I later took them for a post-run spin after a long weekend run (long for not having run in 5 weeks due to injury). While relaxing for an entire afternoon with these socks on, my legs felt fantastic. I've never worn compression socks post-run before, but with my longer training runs coming up, this may be something that I'll have to change about my routine.

Long story short, [get these. Get them now][AmazonLink]. I can't recommend them enough!

[myfoot]: /images/reviews/yorkberg_myfoot.jpg
[packaging]: /images/reviews/yorkberg_packaging.jpg
[AmazonLink]: http://smile.amazon.com/gp/product/B00RY0OCP8/ref=oh_aui_search_detailpage?ie=UTF8&psc=1
