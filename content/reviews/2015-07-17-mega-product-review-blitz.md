Title: Mega Product Review Blitz! (part 1)
Date: 2015-07-17 11:17
Modified: 2015-07-19 12:52
Tags: review, EyeMask,USB,cables,accessories
Series: Mega Product Review Blitz
social_image: reviews/EyeMask.jpg

<!-- PELICAN_BEGIN_SUMMARY -->

OK.  I've been a bit delinquent on some of my product reviews, so I'm going to bundle a bunch of them together in this article. I've posted reviews to Amazon, but not everyone checks there (or here, for that matter).

# <!-- PELICAN_END_SUMMARY -->
## Zakix® 6 Pack, Premium Micro USB Cable Pack
One can never have too many MicroUSB cables, right? Well, you can, but the issue is that, even though they are standardized, not all of them work as well as other. I was hesitant to try out this pack of 3-ft and 6-ft cables, as I've had mixed results with third-party cables on my phones, etc. in the past. Let me say that I was pleasantly surprised.

What do I like about these cables?
- The quality is actually pretty good. They're not the highest quality, but they definitely don't feel like junk. I could see these taking a good toss around in my car, laptop bag, etc.
- They work. That's the important thing. They work. All devices I've tried them with (phones, tablets, battery packs), charge as well, if not better than, some of the cables they came with.
- Convenience in a six-pack. As I said, one can't have too many cables, but having a good set of quality ones of different sizes makes this pack very worthwhile.

I'd highly recommend picking up a pack as a spare set. You never know when you're going to misplace yours. Get them at [Amazon][ZakixCables].

--------------------------------------------------------------------------------

## Soft Cotton & Silk Blend Eye Mask
Really, Larry? You're reviewing a sleep mask? YES!

I've worn plenty in the past, and I find myself occasionally being bothered during a nap (or if I go to bed a bit early) by light. I like it to be dark when I sleep, and a mask is helpful.

One thing about many masks is that they go right up against your eyes and face, with no room for the natural curves, etc. This has always been a problem for me, as I have long eyelashes that touch most sleep masks, and my nose is not the smallest :)  This mask solves both of those issues.

![Inline_EyeMask][EyeMask]

The part that actually covers the eyes has some indentations. Not once after I put on the mask did my eyelashes feel as though they were being pressed upon.

The same with the nose. That piece is curved out to at least the basic shape of the bridge and length of the common nose. Nothing pressing uncomfortably here, either.

The only thing I'd say I did not like was the band. It's a velcro band, which is nice, but it's a bit short for people with big heads (I'm border-line), and the velcro can be slightly uncomfortable if pressed on while sleeping.

Beyond that, I'd say to definitely pick one up from [Amazon][SleepMask] if you are in need of a sleep mask.

--------------------------------------------------------------------------------

Disclosure: I received one or more of the products mentioned above in exchange for review from Giveaway Service website. Regardless, I only recommend products or services I use personally and believe will be good for my readers.

* Reference ID: pmc91ef42552e1027b5d648beeffa1be50
* Reference ID: pmb6a243747ce4d20eaf2cf025e7176662

[zakixcables]: http://smile.amazon.com/Zakix%C2%AE-Pack-Premium-Micro-Cable/dp/B00UWPX3KW/ref=cm_cr_pr_product_top?ie=UTF8
[sleepmask]: http://www.amazon.com/gp/product/B00Y7ZG44U
[EyeMask]: /images/reviews/EyeMask.jpg
