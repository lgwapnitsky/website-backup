Title: Jumping for joy over jump ropes!
Date: 2015-04-16
Modified: 2015-04-25
Tags: review, sports, jumprope
social_image: reviews/jumprope_15min.png

<!-- PELICAN_BEGIN_SUMMARY -->

Let's start this off by saying I'm not very good at jumping rope. I've never been good at it, and I'm relatively clumsy\uncoordinated. A few years ago, I tried again and was rather mediocre at it. Over the past 6 months, I started again, and have found I'm not too bad at it, but still need work.

So, when I was given the opportunity to review two different ropes, I (pardon the bad pun) jumped at the opportunity.

![Inline_jumpropes][jumpropes]

<!-- PELICAN_END_SUMMARY -->

Boson Sport Jump Rope
---------------------

![Inline_boson1][boson1]

First off, was the rope from [Boson Sport][AmazonLink_boson]. While this one reminded me of the rope I got years ago from a major retailer, there were some big differences that made this one superior.

*Pros*

+ Adjustable length - great for switching between us average-sized folk and our friends
+ cable was very strong - much like my computer cable lock
+ soft grip that was very easy to grasp
+ Holy cow, this thing is fast!

*Cons*

- While the grip was soft, I found that it absorbed a bit much of the heavy sweating that I was doing. Long-term, this may get soaked, but so far, it's not horrible

As you can see, very few cons. This has made it into my gym bag as a regular rope.

Inspired Fitness High-Speed Double Under Jump Rope
--------------------------------------------------

![Inline_inspired1][inspired1]

Second, we have the rope from [Inspired Fitness][AmazonLink_Inspired]. The name scared me, and my initial opinion based on the image was that this was going to be VERY different. Boy, was I in for a shock.

*Pros*

+ This thing is durable! The material is almost identical to that of my computer cable lock. I don't think this will be at risk of breaking any time soon
+ Easily adjustable

*Cons*

+ I like the adjustable ends, but the rope seemed to waver from the handles a lot while jumping. Maybe it's my technique...
+ The rope, for someone with my poor vision, can be tough to see. Many a time I was unable to see it while it was moving at high speed
+ Speaking of speed, this was FAST! Almost too fast for my current abilities.
+ Handles were not the most comfortable - they take some getting used to.

I've shelved this rope for now, but plan on trying it again when I get better at jumping. Don't you worry, Inspired rope...you're gonna get reviewed again :)



[AmazonLink_boson]: http://smile.amazon.com/Fitness-Cardio-Crossfit-Training-Suitable/dp/B00PKKFVG8/ref=sr_1_1?s=sports-and-fitness&ie=UTF8&qid=1429982098&sr=1-1&keywords=boson+jump+rope
[AmazonLink_Inspired]: http://smile.amazon.com/gp/product/B00SJ7TZ1G/ref=cm_cr_ryp_prd_ttl_sol_13

[inspired1]: /images/reviews/inspired_fitness_1.jpg
[boson1]: /images/reviews/boson_1.jpg
[jumpropes]: /images/reviews/jumprope_15min.png
