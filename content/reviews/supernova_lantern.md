Title: Supernova 300 LED Lantern - Review
Date: 2015-04-11
Modified: 2015-04-11
Tags: review, tools, lighting
social_image: reviews/supernova300_led_lantern.jpg

<a href="http://www.tomoson.com/?code=TOP92515da4282ce82fb85b60460382a68e" rel="nofollow"><img style="display: none" src="http://www.tomoson.com/images/front/pixel.png" /></a>
<!-- PELICAN_BEGIN_SUMMARY -->

![Inline_lantern1][lantern_amazon]

In the past few years living in the Northeast United States, I've experienced my fair share of blackouts. I have two older fluorescent lanterns that I've used for camping that have come in handy, but their light is slowly dimming with age. 

When I was given the opportunity to review this lantern, I jumped at it. LEDs tend to stay bright for a long time, barely put any drain on the batteries, and are just more environmentally friendly than fluroescent bulbs.

<!-- PELICAN_END_SUMMARY -->

OK...so I did not have a blackout in which to test this lantern. I guess that's both a good and a bad thing. Yet, I gave this thing a good run for its money during the time I've had it.

Test #1 was using it while shoveling snow. "Really?" Yes. I actually had this hooked on the top of my salt spreader. It was bright engough, and put off enough forward-facing light, that I was able to use this almost as a headlight. Moreso, it was easier to see where I had already spread salt on my driveway so I would not go over it too many times.

Test #2 was looking for things in my basement. I'm a mess, my basement is a mess, and, until I clean it all up, finding things can be difficult. I have piles and poor light in my work area, and my [headlamp] sometimes gets in the way under tables (but I do love it). I placed this on the floor under my workbench and was pleasantly suprised that I could clearly see through the piles of stuff to find what I needed.

It's weird to say this, but I'm looking forward to my next blackout so I can test this in those situations. With two small kids, one may want a light on at all times at night, and the older lanterns don't do a great job of that. I'll be happy to update this once that occurs.

In the meantime, I recommend getting at least one for emergencies from [Amazon][AmazonLink].

<a href="http://www.tomoson.com/?code=BOTTOM92515da4282ce82fb85b60460382a68e" rel="nofollow"><img style="display: none" src="http://www.tomoson.com/images/front/pixel.png" /></a>

[lantern_amazon]: /images/reviews/supernova300_led_lantern.jpg
[AmazonLink]: http://smile.amazon.com/Supernova-300-Camping-Emergency-Lantern/dp/B00CCRXCH2/ref=sr_1_1?ie=UTF8&qid=1428773230&sr=8-1&keywords=supernova+300
[headlamp]: /posts/2015/Mar/18/aennon-multi-function-led-headlamp.html

