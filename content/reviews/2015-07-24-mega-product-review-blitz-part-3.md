Title: Mega Product Review Blitz! (part 3)
Date: 2015-07-24
Modified: 2015-07-21
Tags: review, Aeropress, coffee, running, accessories, kitchen, shower, bathroom
Series: Mega Product Review Blitz
social_image: reviews/blitzreview3collage.jpg

<!-- PELICAN_BEGIN_SUMMARY -->
<a href="http://www.tomoson.com/?code=TOP9428f9776a62fb1f566d8089591a2109" rel="nofollow"><img style="display: none" src="http://www.tomoson.com/images/front/pixel.png" /></a>
<a href="http://www.tomoson.com/?code=TOP755eb9d26ddd5758cb601666436cc0f8" rel="nofollow"><img style="display: none" src="http://www.tomoson.com/images/front/pixel.png" /></a>

OK.  I've been a bit delinquent on some of my product reviews, so I'm going to bundle a bunch of them together in this article. I've posted reviews to Amazon, but not everyone checks there (or here, for that matter).

Now time for **Blitz Review #3!!**

![Inline_collage][blitzreview3collage]

<!-- PELICAN_END_SUMMARY -->

## GioSport Running Belt

As a runner, I tend to need to carry important things with me - my keys, phone, ID, money, etc. Running with this stuff in my pockets is not comfortable, and the jangly bounce of keys and a wallet can really screw with my pace. That's where the GioSport Running Belt comes into play.

![Inline_GioSportPouch][GioSportImage]

This belt wound up being almost perfect for me (I'll explain). It was easy to get around my waist, cinched tightly with the clips, and felt as though there was nothing around my waist. I added some money, a key, my ID. No issues. Then it came to my phone.

This pouch is designed to hold the iPhone 6. I own a Nexus 6, which is almost the same size. With my protective case on, I was not able to get the phone in the pouch. Once the case came off, it was good. I wasn't too worried, as I have a glass protector (and insurance) on the phone, but it's something to be conscious of.

One other plus on this pouch is the "porthole". For the most part, this is used for wired headphones to go directly to the phone. I found a better use for it - run a USB charger cable from the phone through the porthole to a battery pack in my backpack. With some simple maneuvering of the wire, it's really easy to get that extra power while out on a long run.

If you're planning on running (or any outdoor activity), and want a good belt\pouch to hold your stuff, definitely pick one of these up from [Amazon][GioSportAmazon].

----

## Aqua Dance Handheld 5 Function Showerhead - Chrome

Easy to install. Took almost no time, and the included Teflon tape was a bonus.

What didn't I like? Pressure seems light. Two of the five settings seemed to do the same thing, and one dribbled water and makes a horribly screeching sound.

I removed the water regulator, and the pressure removed, except on that screeching setting.

Overall, not bad, but I'm not sure if I'd get it again.

[Target][AquaDanceTarget]

----

## Quicklids Silicone Suction Lids and Food Covers

![Inline_quicklids][SiliconeQuicklids]

I'm simply blown away by how good these lids are. Long gone are the days of using an ill-fitting paper plate or paper towels to cover dishes as I'm reheating them. Additionally, these are so much easier to clean AND are environmentally friendly by virtue of being reusable.

If I had a need for more of these, I'd buy them in a heartbeat.

Only thing I'd look for is maybe a smaller version for ramekins\mugs

Tired of making a mess? Get yours on [Amazon][QuickLidsAmazon].

----

## Stainless Aeropress Filter

I love my [Aeropress][Aeropress]. I love it so much, I bought a second one to keep at my parents' house, but, since I'm there so rarely, it's now at my desk in my office so that I don't have to drink from the K-cups. The one thing I've never liked, though, is the paper filters. With one small move of the hand, the filters can get knocked over in their lovely stand and wind up all over the place. Yes, they're cheap, but they're also flimsy. They also don't let the best of the oils, etc. through.  That's where this filter comes in.

![Inline_AFilter][AeropressFilterImage]

Just take a look at this hi-res shot. You can clearly see the fine mesh which lets more oils, yet still very little water (without pressure) through the filter. It's stainless steel, so it's easy to clean and reuse immediately (for those friends\family who want *decaf*). Best of all, since it's reusable, you don't have to worry about filling your garbage with the little paper filters.

Own an Aeropress? [Get one of these filters][AeropressFilter] NOW!

If you don't own an [Aeropress][Aeropress], why not?  Get one!

----

Disclosure: I received one or more of the products mentioned above in exchange for review from Giveaway Service website. Regardless, I only recommend products or services I use personally and believe will be good for my readers.

* Reference ID: pm022823b8aa03cffdb1189da300c18b25
* Reference ID: pmc9509ee01ad2ea5510724dd43d2ea4c5

<a href="http://www.tomoson.com/?code=BOTTOM755eb9d26ddd5758cb601666436cc0f8" rel="nofollow"><img style="display: none" src="http://www.tomoson.com/images/front/pixel.png" /></a>
<a href="http://www.tomoson.com/?code=BOTTOM9428f9776a62fb1f566d8089591a2109" rel="nofollow"><img style="display: none" src="http://www.tomoson.com/images/front/pixel.png" /></a>

[blitzreview3collage]: /images/reviews/blitzreview3collage.jpg
[GioSportAmazon]: http://www.amazon.com/dp/B00WJR6RYA
[GioSportImage]: /images/reviews/GioSportPouch.jpg
[AquaDanceTarget]: http://www.target.com/p/aqua-dance-handheld-5-function-showerhead-chrome/-/A-17217087
[QuickLidsAmazon]: http://smile.amazon.com/gp/product/B00SM6B11G
[SiliconeQuickLids]: /images/reviews/siliconequicklids.jpg
[AeropressFilter]: http://smile.amazon.com/gp/product/B00R26BFZE
[AeropressFilterImage]: /images/reviews/AeropressFilter1.jpg
[Aeropress]: http://smile.amazon.com/Aeropress-Coffee-and-Espresso-Maker/dp/B0047BIWSK/ref=pd_bxgy_79_text_y
