Title: Terry Treetop Saves The Dolphin - REVIEW
Date: 2015-03-06
Modified: 2015-03-06
Tags: review, ebook, children, tali carmi
Description: Review of 'Terry Treetop Saves The Dolphin' by Tali Carmi
social_image: reviews/ebook-terry-dolphin.jpg

<!-- PELICAN_BEGIN_SUMMARY -->
<a href="http://www.tomoson.com/?code=TOP465ea8d6d3f9e22c5fd77c5ba140fec8" rel="nofollow"><img style="display: none" src="http://www.tomoson.com/images/front/pixel.png" /></a>

Another winning series of books from Tali Carmi! The silly adventures of Terry Treetop just light up my daughter's face when we read them!

![Inline_Terry][TerryCover]
<!-- PELICAN_END_SUMMARY -->

Just like any young children's book, Terry Treetop plays on how kids love to dream big, especially when overcoming fears or accomplishing big tasks. This book succeeds in more ways than just that, introducing children to the plight of dolphins, but not in an overly overbearing way. For this, I give Tali Carmi a big thumbs-up.

The best thing about these books is that there is always a good morality lesson that can be easily interpreted by a young child. I know my daughter had been learning a lot!

Pick up your copy from [Amazon]!

<a href="http://www.tomoson.com/?code=BOTTOM465ea8d6d3f9e22c5fd77c5ba140fec8" rel="nofollow"><img style="display: none" src="http://www.tomoson.com/images/front/pixel.png" /></a>

[TerryCover]: /images/reviews/ebook-terry-dolphin.jpg
[Amazon]: http://www.amazon.com/gp/product/B00ONDB72A/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B00ONDB72A&linkCode=as2&tag=largwap0b-20&linkId=SKGUCNDSU5MJUFBP
