Title: 100% Pure Whey Protein - Summit Nutritions - Review
Date: 2015-04-11
Modified: 2015-04-11
Tags: review, nutrition, 
social_image: reviews/pure_whey.jpg

<a href="http://www.tomoson.com/?code=TOP89776568a7ae3a405a4b01f66fada25a" rel="nofollow"><img style="display: none" src="http://www.tomoson.com/images/front/pixel.png" /></a>
<!-- PELICAN_BEGIN_SUMMARY -->

![Inline1][pure_whey]

As I've recently re-added weight-training to my workout routines, I find more of a need for protein immediately after my cross-training workouts. Go ahead, make comments. I do eat protein after my running workouts, but of a different type. Protein shakes have always been my go-to after weights.

When given the opportunity to test out the vanilla Whey Protein from Summit Nutritions, I jumped at it, as I've been loooking for higher quality blends over the years without much success (or lots of money being spent). I'll be honest...I was pleasantly surprised!

<!-- PELICAN_END_SUMMARY -->

To start, I do NOT like vanilla whey protein. I always find that it has a chemical taste that overpowers the vanilla flavoring. When I'm out at the gym, I can't blend in fruit or anything else, so I have to drink it straight. This was the first vanilla protein powder I've had where I did not need to blend any extra flavoring.

My first test was after a cross-training style workout. I wasn't expecting much, but the flavor and the satisfying feeling in my gut were amazing. I had no GI troubles from this (an issue with other protein powders), and didn't feel the need to snack afterward.

The second test was after a run. I needed to try it just to see if I could handle this after the jiggling guts of a few miles. Again, no GI issues and fairly filling. I'll even go so far to say that this may replace some of my regular post-run snacks, or at least supplement ones for the really long training runs.

I've not had a chance to use the third packet due to scheduling, but I'm looking forward to it. I see myself definitely buying this again as my training increases towards marathon distances.

If you want to give it a shot, I recommend getting yours [here][Amazon_Link].

<a href="http://www.tomoson.com/?code=BOTTOM89776568a7ae3a405a4b01f66fada25a" rel="nofollow"><img style="display: none" src="http://www.tomoson.com/images/front/pixel.png" /></a>

[pure_whey]: /images/reviews/pure_whey.jpg
[Amazon Link]: http://smile.amazon.com/Non-Protein-Spiking-Protein-Isolate-Instanized/dp/B00T6Z0ORE/ref=sr_1_1?ie=UTF8&qid=1428772509&sr=8-1&keywords=pure+whey+summit+nutrition
