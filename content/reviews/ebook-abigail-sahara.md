Title: "I am Dreaming About..." by Dan Jackson -REVIEW
Date: 2015-03-14
Modified: 2015-03-14
Tags: review, ebook, children, Dan Jackson
Description: Review of "I am Dreaming About..." by Dan Jackson
social_image: reviews/ebook-dreaming_about.jpg


<!-- PELICAN_BEGIN_SUMMARY -->
<a href="http://www.tomoson.com/?code=TOPc781a8d16d1d45f9a7bd5c1bdd503e25" rel="nofollow"><img style="display: none" src="http://www.tomoson.com/images/front/pixel.png" /></a>

In our house, we love to read. My daughter (4) has lvoed books since she was a little nugget. Any chance we get to read new books makes her excited. When we got this e-book, you can imagine how happy she was. Once we read it, for her, the excitement was partially short-lived.

![Inline_Dreaming][DreamingCover]
<!-- PELICAN_END_SUMMARY -->

The book seemed (when purchased) to be aimed towards someone my daughter's age. It was a nice story about a dog named Max and the dreams he had to be like other animals. Unfortunately, as we were reading it (two different times), my daughter seemed bored with it, and I felt as though the writing was too juvenile for her.

What I do plan on doing is reading this book to my son (9 months). I think he'll like it as it's a nice rhythm of reading, and the pictures are really nice and colorful. Beyond that, this may be a rarity on my tablet.

One bonus in relation to pictures - the ebook comes with printable coloring pages. I plan on loading these up on my computer and printing them for my daughter for a rainy day when her coloring books are full.

Pick up your copy from [Amazon].

> **I received a free copy of this book to review. I was not required to write a positive review nor was I compensated in any other way. The opinions I have expressed are my own. I am disclosing this in accordance with the FTC Regulations.**

<a href="http://www.tomoson.com/?code=BOTTOMc781a8d16d1d45f9a7bd5c1bdd503e25" rel="nofollow"><img style="display: none" src="http://www.tomoson.com/images/front/pixel.png" /></a>

[DreamingCover]: /images/reviews/ebook-dreaming_about.jpg
[Amazon]: http://smile.amazon.com/children-books-dreaming-teaches-adventure-ebook/dp/b00ssxxi28/ref=sr_1_21?ie=utf8&qid=1422436409&sr=8-21&keywords=dan+jackson%20#children&sa-no-redirect=1
