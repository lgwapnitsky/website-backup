Title: Mr. Visibilty LED Running Belt
Date: 2015-02-13 22:03:10
Modified: 2015-02-13 22:03:10
Tags: review, sports, running, safety

<a href="http://www.tomoson.com/?code=TOPc28844a03dc08797ff28b84059d54823" rel="nofollow"><img src="http://www.tomoson.com/images/front/pixel.png" style="display: none" /></a>

<!-- PELICAN_BEGIN_SUMMARY -->
I can't say anything negative about this belt, as it's wonderful! It's
lightweight, doesn't bounce when running,and charges easily. Not to mention it
also stays charged for a good long time.

![Inline_BookCover](/images/reviews/MrVisibilityLEDBeltYellow.jpg)

I put this belt through a few tests:
<!-- PELICAN_END_SUMMARY -->

1. Went for an early morning run near my office. Let's just say that
visiblity was NOT a problem

2. Wore it while shoveling my driveway early one morning. I have a
driveway light, but it doesn't reach all the way to the bottom. Also, with
cars swerving around, it made me easily visible so that I wouldn't get hit
while shoveling.

On a side note, the owner of this business is fantastic. He's been very
reasonable in terms of my review (dealing with sickness in my house), and even
donated a belt for a charity raffle. I'm very happy with the belt and am
highly recommending it to friends and family.

 (note - I was provided not only the raffle item, but another to test in
return for an honest review)

I highly recommend you get yours from [here]!!

<a href="http://www.tomoson.com/?code=BOTTOMc28844a03dc08797ff28b84059d54823" rel="nofollow"><img src="http://www.tomoson.com/images/front/pixel.png" style="display: none" /></a>

[here]: http://www.amazon.com/gp/product/B00PV98ZVG/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B00PV98ZVG&linkCode=as2&tag=largwap0b-20&linkId=AXMADVWZZH63E47Q
