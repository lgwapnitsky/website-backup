Title: Beard Oil from The Real Man Company
Date: 2015-03-07
Modified: 2015-03-07
Tags: review, beard, manliness
social_image: reviews/BeardOilIntro.jpg

<!-- PELICAN_BEGIN_SUMMARY -->
<a href="http://www.tomoson.com/?code=TOP81d843a836e1d56faf5060e9e269eeb8" rel="nofollow"><img style="display: none" src="http://www.tomoson.com/images/front/pixel.png" /></a>
I wasn't sure what to expect when I was given the chance to review Beard Oil from [The Real Man Company][RMC]. Let's just say that I and my beard are very satisfied with what its done for us over the past month!

![Inline1][BeardOil1]

<!-- PELICAN_END_SUMMARY -->

What is beard oil, and what is it for anyway? Well, that was a question I had before I was given the chance to try this stuff out. Well, I found a [great article] on the Huffington Post that made me want to try it. The biggest one for me was:

> ***Beard oil keeps facial hair flake-free and smelling fresh.***

>Thanks to moisturizing oils, a little beard oil is just enough to tame those flyaway hairs and eliminate flakes (a.k.a. beard-druff). And with so many musky/woody essential oils like cedarwood and sandalwood, Schneidman points out that this often overlooked product also acts as a natural cologne.

I've been known to suffer from some 'beard dandruff' in my day, so this really appealed to me. I wash my face regularly and treat it well with good shaving soap and a nice double-edge blade, but sometimes those little flakes on the front of a dark shirt are embarassing.

The first thing I noticed about this product was the aroma (not odor, that makes it sound horrible). I found it to be slightly nutty - a manly, pleasant smell (note, my wife's sensitive nose didn't care for it when freshly applied). When combined with any one of my shaving soaps, particularly the seaweed-based one, my face smelled like heaven to me.

Within a few days, I noticed that the only white in my beard was from the grey hairs that are slowly appearing. The 'beard dandruff' was gone! Not only that, but my beard felt much softer. Pretty soon, the stuff became addictive. Even if running late in the morning, I had to put this stuff on because I knew my beard was benefitting.

![Inline_MeAndBeardOil][BeardOilIntro]

Long-story-short, I will continue using and recommending this product for my hirsute-faced friends and family. I love taking good care of my face and my facial hair, and this is just one more weapon in my aresenal. You should [add it to yours]!


[RMC]: http://www.therealmancompany.com/
[BeardOilIntro]: /images/reviews/BeardOilIntro.jpg
[BeardOil1]: /images/reviews/BeardOil1.jpg
[great article]: http://www.huffingtonpost.com/2014/08/01/beard-oil-facial-hair_n_5629592.html
[add it to yours]:  http://www.amazon.com/gp/product/B00Q33K7YM/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B00Q33K7YM&linkCode=as2&tag=largwap0b-20&linkId=CYSEV5O2YXCXX72M

<a href="http://www.tomoson.com/?code=BOTTOM81d843a836e1d56faf5060e9e269eeb8" rel="nofollow"><img style="display: none" src="http://www.tomoson.com/images/front/pixel.png" /></a>
