Title: Nikkouware Stainless Steel Julienne Vegetable Peeler 
Date: 2015-03-26
Modified: 2015-03-26
Tags: review, kitchen, tools
social_image: reviews/nikkouware_julienne_peeler.jpg

<!-- PELICAN_BEGIN_SUMMARY -->
<a href="http://www.tomoson.com/?code=TOP983e807c5070f49f4ccc24a61a2c0043" rel="nofollow"><img style="display: none" src="http://www.tomoson.com/images/front/pixel.png" /></a>

As if I don't have enough kitchen tools (my drawers are overflowing), I was hesitant to test a vegetable peeler when I already have one that I thought was good enough. After trying this one out, I think I'm a convert to this great peeler from Nikkouware!

![Inline_Peeler1][Peeler_stock]

<!-- PELICAN_END_SUMMARY -->

To start, I loathe peeling vegetables. Carrots are easy, but tedious, potatoes (regular and sweet) are just a pain. Don't even get me started on turnips, rutabagas, or beets! Then this little device wound up entering my house.

The packaging was simple - cardboard box, peeler, and the bonus cleaning brush (yippee!). From the first time I held this peeler in my hand, I knew it was different. The quality felt great, not chintzy. It had a little heft, but nothing more than any other good-quality peeler. The proof of how good this would be was in testing it on sweet potatoes.

The night after I opened the package, I was cooking. I loathe peeling vegetables (yes, I said it twice..that's how much I dislike it), and was weary of how well this straight blade would perform. Let's just say that I was overly impressed. I was able to not only peel the potatoes very quickly, but, if you look at the picture, there are what I affectionately call "eye-gougers" that not only made getting the eyes out of the potatoes, but helped in a few tight spots around the bumps and curves.

I've since used this for a few other veggies (cucumbers, carrots, regular potatoes, a turnip), and haven't turned back to my old peeler since. Cleaning is a breeze with the brush, and being dishwasher safe steel is a big plus (no melting handles).

I think you'll find this to be very a-peel-ing, and highly recommend you pick one up from [Amazon][AmazonLink]!

Note: I was unable to get my pictures for this while writing the article, but will update it soon.

<a href="http://www.tomoson.com/?code=BOTTOM983e807c5070f49f4ccc24a61a2c0043" rel="nofollow"><img style="display: none" src="http://www.tomoson.com/images/front/pixel.png" /></a>

[Peeler_stock]: /images/reviews/nikkouware_julienne_peeler.jpg
[AmazonLink]: http://smile.amazon.com/Premium-Quality-Julienne-Vegetable-Stainless/dp/B00KL90WN0/ref=sr_1_1?ie=UTF8&qid=1427411364&sr=8-1&keywords=nikkouware+julienne+peeler
