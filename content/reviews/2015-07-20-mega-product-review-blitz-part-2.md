Title: Mega Product Review Blitz! (part 2)
Date: 2015-07-20
Modified: 2015-07-21
Tags: review,Bluetooth,headphones,speaker,water bottle,kitchen
Series: Mega Product Review Blitz
social_image: reviews/blitzreview2collage.jpg

<!-- PELICAN_BEGIN_SUMMARY -->

OK.  I've been a bit delinquent on some of my product reviews, so I'm going to bundle a bunch of them together in this article. I've posted reviews to Amazon, but not everyone checks there (or here, for that matter).

Now time for **Blitz Review #2!!**

![Inline_collage][blitzreview2collage]

<!-- PELICAN_END_SUMMARY -->

## ZhiZhu® Bluetooth 4.1 Headphones

I was totally surprised by these headphones. They don't look like the expensive pairs, they don't come packaged like the expensive pairs, yet they sound better than one of the expensive pairs that I have (ahem...Motorola). Yet, the sound out of these definitely rivals my other pair of BT headphones which I absolutely love.

![Inline_ZhiZhu][ZhiZhuHeadphonesImage]

Pros:
* Sound quality is great
* Battery life is great
* Comfortable

Cons:
* I wish the controls were in a better location. It's tough to manage them when they're behind the right side of your neck.

Would I buy and recommend these again?  You bet! Get yours at [Amazon][ZhiZhuHeadphones].

----

## XSBoom Bluetooth Speaker

While we're talking about Bluetooth audio, let me tell you about the [XSBoom Bluetooth Speaker][XSBoomSpeakerAmazon]. For something that's the size of an egg (seriously!), I never expected such good sound out of this. Not only that, but it was so easy to set up! I've had this sitting on my dining room table to listen to music off either my phone or tablet, and the audio is fantastic.

![Inline_XSBoom][XSBoomEgg]

Note that this device does not have audio controls of its own. The volume is somewhat limited by the volume controls of your source device, but it's still pretty significant.

Bonus - if you don't have a Bluetooth device, this comes with an audio cable so you can plug it directly in to your headphone jack.

I just want to buy more of these for everyone to enjoy, but can't afford that, so why not get your own over at [Amazon][XSBoomSpeakerAmazon]??

----

##My Lil Friend Eco-Friendly Silicone Cutting Mats

With limited kitchen space, taking out the large acrylic, glass, wooden and\or plastic cutting boards can really take away from your preparation area and really crowd things up. That's why, when products like this come along, they become life savers.

![Inline_CuttingBoards][CuttingBoards]

These little things are AMAZING. They're thin, easy to store, easy to clean. They've held up well with every knife we've put to them. We've had them in the dishwasher, and the only thing that's happened is that they've curled a little bit.

One of the best uses we have for them is keeping them at my dinner table. We always need to cut food for my 1-year-old son, and, like I mentioned earlier, these are so much better than dragging out an entire cutting board.

Buy these if you have a small kitchen. Buy them if you're a parent cutting small pieced of food. Buy them because they're cool as heck!

[Amazon][CuttingBoardsAmazon]

--------------------------------------------------------------------------------

##Bonké Water Bottle with Fruit Infuser

I've been wanting a water infuser bottle for a long time. I've looked at plenty of stores at ones that are expensive, but none have really met the qualifications for what I want:

* BPA-Free
* NO STRAW!
* Seals well
* Easy to carry (ergonomically friendly)
* Fits in the cupholder in my car

Then I found [this one][BonkeAmazon] made by [Bonké](http://thebonke.com). Pretty much from the day it arrived in the mail, I've been using it. I'm finding myself drinking more water during the day at the office (I go nuts with seltzer at home) now that I'm flavoring it with fresh fruits and vegetables. Some of my favorite infusions so far:

* Kirby cucumber
* Strawberries and lemon
* Lemon and peach
* Radish (yes, seriously! This was SO refreshing!)

When it comes to convenience and quality, this infuser bottle is absolutely incredible. I've referred it to a few friends who've seen me walking around with it. I'm hoping they've bought it, but if not, I think you should!  Get yours at [Amazon][BonkeAmazon], and let them know how awesome it is!

<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="4" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAAGFBMVEUiIiI9PT0eHh4gIB4hIBkcHBwcHBwcHBydr+JQAAAACHRSTlMABA4YHyQsM5jtaMwAAADfSURBVDjL7ZVBEgMhCAQBAf//42xcNbpAqakcM0ftUmFAAIBE81IqBJdS3lS6zs3bIpB9WED3YYXFPmHRfT8sgyrCP1x8uEUxLMzNWElFOYCV6mHWWwMzdPEKHlhLw7NWJqkHc4uIZphavDzA2JPzUDsBZziNae2S6owH8xPmX8G7zzgKEOPUoYHvGz1TBCxMkd3kwNVbU0gKHkx+iZILf77IofhrY1nYFnB/lQPb79drWOyJVa/DAvg9B/rLB4cC+Nqgdz/TvBbBnr6GBReqn/nRmDgaQEej7WhonozjF+Y2I/fZou/qAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://instagram.com/p/4WtXTjIxJe/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_top">Infusion of orange and strawberries vs coffee is an unlikely winner today! #Bonke</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">A photo posted by Larry G. Wapnitsky (@larrygwapnitsky) on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2015-06-25T14:04:47+00:00">Jun 25, 2015 at 7:04am PDT</time></p></div></blockquote>
<script async defer src="//platform.instagram.com/en_US/embeds.js"></script>

----

Disclosure: I received one or more of the products mentioned above in exchange for review from Giveaway Service website. Regardless, I only recommend products or services I use personally and believe will be good for my readers.

* Reference ID: pme624d754286d0aac962f185f1a21809e
* Reference ID: pm739465804a0e17d2a47c9bc9c805d60a
* Reference ID: prcf004fdc76fa1a4f25f62e0eb5261ca3


[blitzreview2collage]: /images/reviews/blitzreview2collage.jpg
[MotorolaBluetoothHeadphones]: http://smile.amazon.com/Motorola-Universal-Bluetooth-Stereo-Headset/dp/B00H0CV9TM/ref=sr_1_1?ie=UTF8&qid=1437345288&sr=8-1&keywords=motorola+bluetooth+headphones
[ZhiZhuHeadphones]: http://www.amazon.com/dp/B00V4EEML4
[ZhiZhuHeadphonesImage]: /images/reviews/zhizhuheadphones.jpg
[XSBoomSpeakerAmazon]: http://smile.amazon.com/gp/product/B00KUTF6ZK?redirect=true&ref_=cm_cr_ryp_prd_ttl_sol_0
[XSBoomEgg]: /images/reviews/XSBoomEgg.jpg
[CuttingBoardsAmazon]: http://smile.amazon.com/Durable-Flexible-Compact-Practical-Kitchen/dp/B00X4JGSI2?sa-no-redirect=1
[CuttingBoards]: /images/reviews/CuttingBoards.jpg
[BonkeAmazon]: http://smile.amazon.com/Water-Bottle-Fruit-Infuser-Friendly/dp/B00WKFSTVA/ref=sr_1_1?ie=UTF8&qid=1437349557&sr=8-1&keywords=bonke
