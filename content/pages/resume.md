Title: Resume of Larry G. Wapnitsky
Slug: resume
Date: 2015-03-17
Modified: 2015-03-17
Status: hidden


**Larry G. Wapnitsky, MBA**

**Glenside, PA 19038**

**215.498.7136**

**larry@wapnitsky.com**

Highly certified, educated, and skilled **Senior Windows Systems Engineer** with over fourteen years of experience working hands on with Windows systems. Spent four years consulting to organizations of all sizes gaining diverse technical environment and industry experience, including Architecture/Design, Academia, Financial Services, Advertising, Consumer Goods, Telecom, and Government. Trusted by stakeholders to find the best solution while managing multiple projects. Versatile resource confident in a mission-critical environment who fits well in a small to medium-sized organization that values IT as a critical function to support business objectives.

**Operating Systems**: Windows Server 2000/2003/2008, Windows XP/7, Linux
(Debian, RedHat/Centos), Novell NetWare

**Software:** Exchange 2000/2003, VMWare, Python, Visual C\#, postfix,
KiXtart, Microsoft Hyper-V

**Databases:** Microsoft SQL 2000/2008, MySQL

**Protocols:** TCP/IP, LDAP, Active Directory, XMPP, SMTP

**Security:** Veritas BackupExec, EMC Retrospect, ArcServe, Sonicwall
Mail Gateways/Firewalls/VPNs

**Management:** 10 indirect reports and six direct reports mostly consisting
of level 1 and 2 support

###Experience

####**Wallace Roberts & Todd, LLC**, Philadelphia, PA **2008-2012**

#####IT Support & Administration Coordinator

#####*A LEED-certified Architecture/Design firm founded in 1962*

-   Provided desktop and server support for 100 users in 5 locations
    throughout the United States

-   Designed and implemented a combination Python/Linux & C\#/MS Outlook
    system to handle large file attachments in e-mail without a
    significant effect on mailbox quotas as part of 3-point solution.

-   Implemented and maintained SharePoint 2007 site as corporate
    intranet

-   Wrote numerous C\# Outlook add-ins to handle dynamic signature
    taglines, and PST management tools to help reduce the load PST files
    placed on network backups.

-   Enhanced login scripts by using Kixtart to implement modular scripts
    that can be added/removed as needed for testing/diagnostics and
    upgrades.

-   Introduced utilization of PXE booting for diagnostic and system
    cloning utilities, which allowed for quicker rollouts of new
    operating systems to desktops/laptops.

-   Directly involved in recommendation, assessment and annual budgeting
    of new software and hardware.

-   Rolled out numerous upgraded solutions using open-source software,
    effectively reducing the overall projected cost for solutions that
    were currently being examined (faster FTP services, XMPP/Instant
    Messaging, postfix for mail analysis)

-   Maintained multiple servers running on various flavors of virtual
    machines (Microsoft Hyper-V, VMWare, LXC/Linux Containers)

####**Morton Capital Management**, Los Angeles, CA **2003-2008**

#####Senior Systems Administrator

#####*An SEC registered investment adviser founded in 1981 with over \$900M in assets.*

-   Depended upon as the sole technical resource for 20 local and remote
    users.

-   Designed a server/SAN-based disaster recovery solution using Linux,
    Windows, VMWare and NSI Doubletake on a limited budget of \$30K.

-   Encouraged and managed the conversion from paper to digital
    operations, communication, and storage in compliance with SEC Rule
    17a-4 using LaserFiche.

-   Reduced system configuration from 30-45 minutes to less than a
    minute with an internal Linux Help Desk system with automated login
    short cuts that also tracked project progress.

-   Received accolades on internal instant messaging system that
    integrated with Active Directory logins using LDAP, which was used
    for faster notification and resolution of technical issues.

-   Created procedures and documentation for file naming and
    troubleshooting.

-   Submitted project plans to COO for budget approval that included 3-4
    potential solutions in order of quality and value, and then
    facilitated demonstrations of top solutions.

-   Successfully migrated from Windows/Exchange 2000 to 2003 with
    complete transparency to all users.

-   Enabled remote access for Mac users, downloaded compatible software,
    and trained on use.

-   Established various backup practices to minimize downtime, such as
    creating dual WAN connections for Internet redundancy, separating
    server roles for maximum failure points, revising record retention
    systems and testing schedules, migrating user files from Windows to
    Linux, and fire/flood-safe storage of tapes.

####**Contact Solutions (now NetFusion)**, Los Angeles, CA **2002-2003**

#####Systems Consultant

#####*A newly founded consulting firm providing .NET and web services, architecture, development and integration.*

-   Managed a team of six level 1 and 2 support representatives in
    addition to remote support staff.

-   Selected to lead teams of 3-4 consultants in the migration of
    NetWare to Windows 2000 server migrations in 15 cities stretching
    from California to Michigan for 500 MassMutual Insurance users.

-   Interfaced with on-site contacts for scheduling and monitoring of
    reconfiguration tasks and data migrations.

-   Implemented Exchange 2000 servers and supported Windows NT 4.0/2000
    and NetWare 5 servers.

-   Set up appropriate DNS records for new systems and host name
    resolution for web-based access.

-   Maintained and validated quality backups for emergency restoration
    with Veritas BackupExec and ArcServe and set up, supported NetScreen
    5xp firewalls, and implemented and maintained McAfee AntiVirus.

-   Integrated Macintosh workstations into Windows 2000 environment.

-   Troubleshot Bloomberg workstation for an accounting firm.

####**Sprint E|Solutions (defunct)**, New York, NY & Los Angeles, CA **1998-2001**

#####Technical Solutions Consultant

#####*A Sprint and Paranet joint venture that upgraded, implemented, hosted, and managed eBusiness solutions.*

-   Wrote statements of work (SOWs) and attended pre-sales meetings to
    answer clients. questions.

-   Developed Best Practices documentation for Windows 2000 migrations,
    enabling sales people to easily assess needs and adjust prospect
    presentations.

	**Unilever NA**

	-   Provided Level 3 Windows support on a team for 1400 users and 150
    servers in a national data center.

	-   Integrated Hyperion with business development and financial planning at UnileverNA/Lipton headquarters, which improved the monitoring of supply chain management.

	-   Saved company resources by collaborating with software rollout team to develop a system that could be installed remotely on every computer and by working with internal software developers on a web-based digital repository for paper-based documentation.

	-   Led a nationwide rollout of Compaq Insight Manager to all servers that involved setting up monitoring at the regional data center and training monitor techs.

	-   Administered SMS package rollouts and Citrix servers for all users.

	-   Worked with backup team to assist in Legato install and backups.

	**Texas State Government**

	-   Remotely tested the security of Windows systems using ISS, port scanners, and nmap.

	-   Exploitations were attempted and were tested through social engineering.

	**Sprint**

	-   Managed the migration of a small wireless ISP into the Sprint Broadband Wireless organization.

	-   Set up all users with e-mail accounts and trained them on new access
    procedures.

	**American Express Travel Services**

	-   Migrated Novell Netware to Windows 2000.

	-   Upgraded POS equipment for travel offices in six cities with 10 users in each.

####**SUNY at Buffalo **, Amherst, NY, **1996 . 1997**

#####Assistant LAN Administrator, Computing and Information Technology

-   Supported hardware and software, such as Eproms, Matlab, Pascal
    Compiler, MS Office, and Lexis/Nexis.

-   Administered Netware and NT servers for various departments and
    about 75 desktops in student labs.

-   Automated software installation and upgrade procedures that ran at
    user logon.

###Education and Certification

**University of Phoenix** MBA, Technology Management

**State University of New York at Buffalo** BA, Computer Science

MCSE, MCP+I
